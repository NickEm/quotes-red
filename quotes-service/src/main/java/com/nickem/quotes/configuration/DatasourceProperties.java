package com.nickem.quotes.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "datasource")
public class DatasourceProperties {

    private DatasourceInitialization initialization;

    private String driverClassName;
    private String host;
    private String port;
    private String jdbcUrl;
    private String name;
    private String username;
    private String password;

    private String poolName;
    private int proxyPort;
    private int maximumPoolSize;
    private int minimumIdle;
    private int connectionTimeout;

    private String dialect;

    @Data
    static class DatasourceInitialization {
        private boolean enabled;
        private boolean parseCsv;
    }

}
