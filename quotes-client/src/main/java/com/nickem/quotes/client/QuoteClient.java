package com.nickem.quotes.client;

import com.nickem.quotes.domain.dto.QuoteRequest;
import com.nickem.quotes.domain.dto.QuoteResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Collection;
import java.util.function.Function;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;
import reactor.netty.tcp.TcpClient;

@Slf4j
@Component
public class QuoteClient {

    private final Function<TcpClient, TcpClient> tcpClientTcpClientFunction = client -> client.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 20000)
                                                                                              .doOnConnected(conn -> conn.addHandlerLast(
                                                                                                  new ReadTimeoutHandler(20))
                                                                                                                         .addHandlerLast(
                                                                                                                             new WriteTimeoutHandler(
                                                                                                                                 20)));

    private final HttpClient httpClient = HttpClient.create().tcpConfiguration(tcpClientTcpClientFunction);
    private final ClientHttpConnector connector = new ReactorClientHttpConnector(httpClient);
    private final WebClient client = WebClient.builder()
                                              .baseUrl("http://localhost:8090")
                                              .clientConnector(connector)
                                              .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                                              .build();

    public Mono<QuoteResponse> create(final QuoteRequest quote) {
        return client.post().uri("/quotes").body(Mono.just(quote), QuoteRequest.class).retrieve().bodyToMono(QuoteResponse.class);
    }

    public Flux<QuoteResponse> getRandomExcept(final Collection<Long> exceptIds) {
        return client.get()
                     .uri(builder -> builder.path("/quotes/random").queryParam("exceptIds", exceptIds).build())
                     .retrieve()
                     .bodyToFlux(QuoteResponse.class);
    }

    public Mono<QuoteResponse> getById(final Long quoteId) {
        return client.get().uri("/quotes/" + quoteId).retrieve().bodyToMono(QuoteResponse.class);
    }

}
