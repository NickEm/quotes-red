package:
	./gradlew build -x test

boot-image:
	./gradlew :quotes-service:clean :quotes-service:bootBuildImage

quotes-up-db:
	docker-compose -f docker-compose.yml up quotes-red-postgres

quotes-service-build:
	cd quotes-service && docker build -f Dockerfile -t quotes-service . && cd ../

quotes-service-image:
	if [ ! -n "$v" ]; then v="latest"; fi; \
    echo "building with tag $$v"; \
	cd quotes-service && docker build -f Dockerfile -t quotes-red-service:$v . && docker tag quotes-red-service:$v nickem/quotes-red-service:$v

quotes-service-image-push: quotes-service-image
	docker push nickem/quotes-red-service:$v

quotes-up-full:
	docker-compose -f docker-compose.yml up --build

quotes-up-tiny:
	docker-compose -f docker-compose-tiny.yml up
