create schema if not exists quotes_red;

drop table if exists quotes_red.quotes;
create table if not exists quotes_red.quotes
(
    record_id    serial,
    content      varchar(4096)               null,
    author       varchar(64)                 null,
    tags         varchar(256)                null,
    language     varchar(6)                  not null default 'en',
    created_date timestamp without time zone not null default now(),
    state        varchar(16)                 not null default 'ACTIVE',
    primary key (record_id)
);
