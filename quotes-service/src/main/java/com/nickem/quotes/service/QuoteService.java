package com.nickem.quotes.service;

import com.nickem.quotes.configuration.DefectConfiguration;
import com.nickem.quotes.domain.dto.QuoteRequest;
import com.nickem.quotes.domain.dto.QuoteResponse;
import com.nickem.quotes.domain.entity.QuoteState;
import com.nickem.quotes.entity.Quote;
import com.nickem.quotes.repository.QuoteRepository;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@Slf4j
@Service
@RequiredArgsConstructor
public class QuoteService {

    private final List<Long> returnedQuoteIds = new CopyOnWriteArrayList<>();

    private final QuoteRepository quoteRepository;
    private final ModelMapper modelMapper;
    private final DefectConfiguration defectConfiguration;


    public Mono<QuoteResponse> create(final QuoteRequest quoteRequest) {
        final Quote quote = modelMapper.map(quoteRequest, Quote.class);
        quote.setCreatedDate(Date.from(Instant.now()));
        return quoteRepository.save(quote)
                              .map(e -> modelMapper.map(e, QuoteResponse.QuoteResponseBuilder.class).build());
    }

    public Flux<QuoteResponse> getRandom(final List<Long> exceptIds) {
        return quoteRepository.findByQuoteStateAndIdNotInOrderBy(QuoteState.ACTIVE, exceptIds, 1)
                              .doOnNext(injectDefect())
                              .switchIfEmpty(
                                      Mono.error(new ResponseStatusException(NOT_FOUND, "Unable to find random quote")))
                              .map(quote -> modelMapper.map(quote, QuoteResponse.QuoteResponseBuilder.class).build());
    }
    
    private Consumer<Quote> injectDefect() {
        return q -> {
            if (defectConfiguration != null && (defectConfiguration.isLatencyOn() || defectConfiguration.isErrorOn())) {
                if (defectConfiguration.isLatencyOn()) {
                    if (returnedQuoteIds.size() > 0 &&
                        returnedQuoteIds.size() % defectConfiguration.getLatencyRate() == 0) {
                        try {
                            Thread.sleep(defectConfiguration.getLatencyValueMs());
                        } catch (Exception e) {
                            returnedQuoteIds.add(q.getId());
                            throw new RuntimeException("Sleep is interrupted", e);
                        }
                    }
                }

                if (defectConfiguration.isErrorOn()) {
                    if (returnedQuoteIds.size() > 0 && returnedQuoteIds.size() % defectConfiguration.getErrorRate() == 0) {
                        returnedQuoteIds.add(q.getId());
                        throw new RuntimeException("Ups :( Something went wrong ...");
                    }
                }
                returnedQuoteIds.add(q.getId());
            }
        };
    }

    public Mono<QuoteResponse> getById(final Long quoteId) {
        return quoteRepository.findById(quoteId)
                              .switchIfEmpty(Mono.error(new ResponseStatusException(NOT_FOUND, String.format(
                                      "Unable to find quote [%s]", quoteId))))
                              .map(e -> modelMapper.map(e, QuoteResponse.QuoteResponseBuilder.class).build());
    }

}
