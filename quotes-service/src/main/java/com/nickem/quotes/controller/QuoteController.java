package com.nickem.quotes.controller;

import com.nickem.quotes.domain.dto.QuoteRequest;
import com.nickem.quotes.domain.dto.QuoteResponse;
import com.nickem.quotes.service.QuoteService;

import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/quotes")
public class QuoteController {

    private final QuoteService quoteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<QuoteResponse> create(@RequestBody final QuoteRequest quote) {
        return quoteService.create(quote);
    }

    @GetMapping("/random")
    private Flux<QuoteResponse> getRandom(
            @RequestParam(value = "exceptIds", required = false) final List<Long> exceptIds) {
        return quoteService.getRandom(CollectionUtils.isEmpty(exceptIds) ? Collections.singletonList(0L) : exceptIds);
    }

    @GetMapping("/{quoteId}")
    private Mono<QuoteResponse> getById(@PathVariable final Long quoteId) {
        return quoteService.getById(quoteId);
    }

}
