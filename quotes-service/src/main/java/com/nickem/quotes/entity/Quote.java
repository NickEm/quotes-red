package com.nickem.quotes.entity;

import com.nickem.quotes.domain.entity.QuoteState;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table("quotes")
public class Quote {

    @Id
    @Column("record_id")
    private Long id;

    @Column("content")
    private String content;

    @Column("author")
    private String author;

    @Column("language")
    private String language;

    @Column("tags")
    private String tags;

    @Column("created_date")
    private Date createdDate;

    @Column("state")
    private QuoteState state;

}
