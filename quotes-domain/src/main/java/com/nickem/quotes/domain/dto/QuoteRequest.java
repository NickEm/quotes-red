package com.nickem.quotes.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.nickem.quotes.domain.entity.QuoteState;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@JsonDeserialize(builder = QuoteRequest.QuoteRequestBuilder.class)
public class QuoteRequest implements Serializable {

    @JsonProperty("content")
    private String content;

    @JsonProperty("author")
    private String author;

    @JsonProperty("language")
    private String language;

    @JsonProperty("state")
    private QuoteState state;

    @Builder
    public QuoteRequest(final String content, final String author, final String language, final QuoteState state) {
        this.content = content;
        this.author = author;
        this.language = language;
        this.state = state;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public final static class QuoteRequestBuilder {}

}
