package com.nickem.quotes.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "defect-injection")
public class DefectConfiguration {
    
    private boolean latencyOn;
    private int latencyRate;
    private int latencyValueMs;

    private boolean errorOn;
    private int errorRate;
    
}
