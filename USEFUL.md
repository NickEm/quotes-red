https://hub.docker.com/r/prom/prometheus/

http://127.0.0.1:8090/actuator/prometheus

https://www.opsian.com/blog/performance-test-spring-boot-gatling/


* `./gradlew :quotes-service:clean :quotes-service:build && cd quotes-service && docker build -f Dockerfile -t quotes-red-sercvice . && cd ../`
* `./gradlew :quotes-service:clean :quotes-service:build :quotes-service:shadowJar`


```
ab -n 10000 -c 200 http://localhost:8090/quotes/random
```
```
time ab -n 10000 -c 100 http://localhost:8090/quotes/random
time ab -n 10000 -c 200 http://192.168.64.2:30090/quotes/random
```


```bash
start=`date +%s`
stuff
end=`date +%s`

runtime=$((end-start))

```


#### Grafana + Prometheus
* https://stackabuse.com/monitoring-spring-boot-apps-with-micrometer-prometheus-and-grafana/
* https://nirajsonawane.github.io/2020/05/03/monitoring-spring-boot-application-with-prometheus-and-grafana/
* [defining-custom-metrics](https://blog.autsoft.hu/defining-custom-metrics-in-a-spring-boot-application-using-micrometer/)
* [profiling-with-custom-metrics](https://dzone.com/articles/monitoring-and-profiling-spring-boot-application)

* [with-kubernetes](https://blog.autsoft.hu/monitoring-microservices/)

```shell
./gradlew :quotes-service:clean :quotes-service:bootBuildImage
```

#### Spring boot docker images
* https://reflectoring.io/spring-boot-docker/
* https://www.baeldung.com/spring-boot-docker-images
* https://spring.io/guides/topicals/spring-boot-docker/


#### Vagrant
```
vagrant upload ../kubernetes/quotes-red-service-dep.yml /tmp/quotes-red/quotes-red-service-dep.yml k8s-1

vagrant upload ../kubernetes/quotes-red-db-dep.yml /tmp/quotes-red/quotes-red-db-dep.yml k8s-1
```

#### Minikube

* [Setup](https://gist.github.com/kevin-smets/b91a34cea662d0c523968472a81788f7)
* [Issue with minikube](https://github.com/kubernetes/minikube/issues/7344)
  * `minikube start --vm-driver=hyperkit`
* [Expose service](https://stackoverflow.com/questions/49219171/expose-service-on-local-kubernetes/60111530#60111530)

```shell
minikube delete
minikube ip
minikube ssh
minikube start --vm-driver=hyperkit
eval $(minikube docker-env) - to push local docker image to minikube
minikube service list
```

#### Kubernetes

```shell
kubectl create ns quotes-red

cd kubernetes

kubectl config set-context --current --namespace=quotes-red

kubectl convert -f . | kubectl create -f -
kubectl apply -f .
kubectl delete -f .
```

`docker images --no-trunc --quiet docker.io/nickem/quotes-red-service:latest`
`docker images --no-trunc --quiet nickem/quotes-red-service:latest`