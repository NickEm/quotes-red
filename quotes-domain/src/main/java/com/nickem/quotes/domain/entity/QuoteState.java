package com.nickem.quotes.domain.entity;

public enum QuoteState {
    ACTIVE,
    CREATION,
    PENDING,
    INACTIVE,
    CANCELED,
    DRAFT
}
