package com.nickem.quotes.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.nickem.quotes.domain.entity.QuoteState;

import java.util.Date;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@Data
@JsonDeserialize(builder = QuoteResponse.QuoteResponseBuilder.class)
@ToString(callSuper = true)
public class QuoteResponse extends QuoteRequest {

    @JsonProperty("id")
    private final Long id;

    @JsonProperty("createdDate")
    private final Date createdDate;


    @Builder(builderMethodName = "quoteBuilder")
    public QuoteResponse(final String content, final String author, final String language, final QuoteState state,
                         final Long id, final Date createdDate) {
        super(content, author, language, state);
        this.id = id;
        this.createdDate = createdDate;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class QuoteResponseBuilder {}

}
