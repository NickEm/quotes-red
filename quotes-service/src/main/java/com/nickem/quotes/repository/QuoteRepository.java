package com.nickem.quotes.repository;

import com.nickem.quotes.entity.Quote;
import com.nickem.quotes.domain.entity.QuoteState;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import reactor.core.publisher.Flux;

@Repository
public interface QuoteRepository extends ReactiveSortingRepository<Quote, Long> {

    @Query(value = "SELECT * FROM quotes q WHERE q.state = :state AND q.record_id NOT IN (:ids) ORDER BY RANDOM() " +
                   "LIMIT :limit")
    Flux<Quote> findByQuoteStateAndIdNotInOrderBy(@Param("state") final QuoteState state, @Param("ids") final List<Long> ids,
                                                  @Param("limit") final int limit);

}