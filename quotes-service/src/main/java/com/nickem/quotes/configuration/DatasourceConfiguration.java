package com.nickem.quotes.configuration;

import com.nickem.quotes.entity.Quote;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;
import org.springframework.data.r2dbc.connectionfactory.R2dbcTransactionManager;
import org.springframework.data.r2dbc.connectionfactory.init.CompositeDatabasePopulator;
import org.springframework.data.r2dbc.connectionfactory.init.ConnectionFactoryInitializer;
import org.springframework.data.r2dbc.connectionfactory.init.ResourceDatabasePopulator;
import org.springframework.data.r2dbc.convert.R2dbcCustomConversions;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;
import org.springframework.transaction.ReactiveTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import io.r2dbc.spi.ConnectionFactories;
import io.r2dbc.spi.ConnectionFactory;
import io.r2dbc.spi.ConnectionFactoryOptions;
import io.r2dbc.spi.Option;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import static io.r2dbc.spi.ConnectionFactoryOptions.DATABASE;
import static io.r2dbc.spi.ConnectionFactoryOptions.PASSWORD;
import static io.r2dbc.spi.ConnectionFactoryOptions.USER;

@Configuration
@RequiredArgsConstructor
@EnableR2dbcRepositories(basePackages = "com.nickem.quotes.repository")
@EntityScan(basePackages = "com.nickem.quotes.entity")
@EnableTransactionManagement
@Slf4j
public class DatasourceConfiguration extends AbstractR2dbcConfiguration {

    private final DatasourceProperties properties;

    @Override
    @Bean("connectionFactory")
    public ConnectionFactory connectionFactory() {
        final ConnectionFactoryOptions baseOptions = ConnectionFactoryOptions.parse(properties.getJdbcUrl());
        return ConnectionFactories.get(ConnectionFactoryOptions.builder()
                                                               .from(baseOptions)
                                                               .option(USER, properties.getUsername())
                                                               .option(PASSWORD, properties.getPassword())
                                                               .option(DATABASE, properties.getName())
                                                               .option(Option.valueOf("schema"), properties.getName())
                                                               .option(Option.valueOf("search_path"), properties.getName())
                                                               .build());
    }


    @ReadingConverter
    static class ReadDateConverter implements Converter<LocalDateTime, Date> {

        @Override
        public Date convert(final LocalDateTime time) {
            return Date.from(time.atZone(ZoneId.systemDefault()).toInstant());
        }
    }

    @Bean
    @Override
    public R2dbcCustomConversions r2dbcCustomConversions() {
        final List<Converter<?, ?>> converterList = List.of(new ReadDateConverter());
        return new R2dbcCustomConversions(getStoreConversions(), converterList);
    }

    @Bean
    public ReactiveTransactionManager transactionManager(@Qualifier("connectionFactory") final ConnectionFactory connectionFactory) {
        return new R2dbcTransactionManager(connectionFactory);
    }

    @Bean
    @ConditionalOnProperty(value = "datasource.initialization.enabled", havingValue = "true")
    public ConnectionFactoryInitializer initializer(@Qualifier("connectionFactory") final ConnectionFactory connectionFactory) throws
            IOException {
        log.debug("Initializing datasource...");
        final ConnectionFactoryInitializer initializer = new ConnectionFactoryInitializer();
        initializer.setConnectionFactory(connectionFactory);
        final CompositeDatabasePopulator populator = new CompositeDatabasePopulator();
        populator.addPopulators(new ResourceDatabasePopulator(new ClassPathResource("db/schema.sql")));

        final var classPathResource = new ClassPathResource("db/data.sql");
        if (properties.getInitialization().isParseCsv()) {
            log.debug("Parsing csv file to fill datastore.");
            final List<Quote> quotes = parse(new ClassPathResource("quotes-database.csv").getFile(), ';');
            final var insertStm = prepareInsert(quotes);
            writeQuotesToResource(insertStm, classPathResource);
        }

        populator.addPopulators(new ResourceDatabasePopulator(classPathResource));
        initializer.setDatabasePopulator(populator);
        return initializer;
    }

    private void writeQuotesToResource(final String insertStm, final ClassPathResource classPathResource) throws
            IOException {
        final byte[] strToBytes = insertStm.getBytes();
        Files.write(classPathResource.getFile().toPath(), strToBytes);
    }

    private static List<Quote> parse(final File file, final char separator) {
        final List<Quote> result;
        try {
            final FileReader filereader = new FileReader(file);
            final CSVParser parser = new CSVParserBuilder().withSeparator(separator).build();
            final CSVReader csvReader = new CSVReaderBuilder(filereader).withCSVParser(parser).build();

            final List<String[]> allLines = csvReader.readAll();

            final CsvColumnMapper mapper = new CsvColumnMapper(allLines.get(0));

            result = allLines.stream().skip(1).map(fields -> Quote.builder()
                                                              .content(fields[mapper.getIndexByFieldName("QUOTE")])
                                                              .author(fields[mapper.getIndexByFieldName("AUTHOR")])
                                                              .tags(fields[mapper.getIndexByFieldName("GENRE")])
                                                              .build())
                             .limit(10000)
                             .collect(Collectors.toList());
        } catch (final Exception e) {
            final var msg = "Error while parsing csv file";
            log.error(msg, e);
            throw new IllegalArgumentException(msg, e);
        }

        return result;
    }

    private static String prepareInsert(final Collection<Quote> quotes) {
        final var insertPattern = "('%s', '%s', '%s')";
        final var builder =
                new StringBuilder("INSERT INTO quotes_red.quotes (content, author, tags) VALUES \n");

        builder.append(quotes.stream()
                             .map(quote -> String.format(insertPattern,
                                                         sanitizeInsertValue(quote.getContent()),
                                                         sanitizeInsertValue(quote.getAuthor()),
                                                         sanitizeInsertValue(quote.getTags())))
                             .collect(Collectors.joining(",\n")));

        builder.append(";");
        return builder.toString();
    }

    private static class CsvColumnMapper {

        private final Map<String, Integer> fieldToIndexMapper = new HashMap<>();

        public CsvColumnMapper(final String[] headers) {
            for (int i = 0; i < headers.length; i++) {
                fieldToIndexMapper.put(headers[i], i);
            }
        }

        public int getIndexByFieldName(final String fieldName) {
            return fieldToIndexMapper.get(fieldName);
        }
    }


    private static String sanitizeInsertValue(final String value) {
        final var quoteReplace = StringUtils.replace(value, "'", "''");
        return StringUtils.replace(quoteReplace, "$", "[$]");
    }


}
